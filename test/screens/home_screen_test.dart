import 'package:flutter_test/flutter_test.dart';
import 'package:otp_task/model/word_model.dart';

void main() {
  test('if 5 letter long word is added then score should be 5 ', () async {
    const word = Word(text: 'apple', score: 5);
    expect(word.score, 5);
  });
}
