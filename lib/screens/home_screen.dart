import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otp_task/blocs/bloc/words_bloc.dart';
import 'package:otp_task/model/word_model.dart';

import 'add_word_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int totalScore = 0;

  void updateInformation(int information) {
    setState(() => totalScore += information);
  }

  void moveToSecondPage() async {
    final information = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const AddWordScreen()),
    );
    updateInformation(information);
  }

  @override
  Widget build(BuildContext context) {
    List<Word> myList = [];
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white10,
        title: const Text(
          'W O R D S',
          style: TextStyle(color: Colors.black),
        ),
        actions: [
          IconButton(
            onPressed: () {
              moveToSecondPage();
            },
            icon: const Icon(
              Icons.add,
              color: Colors.black,
            ),
          ),
        ],
      ),
      body: BlocBuilder<WordsBloc, WordsState>(
        builder: (context, state) {
          if (state is WordsLoading) {
            return const Center(child: CircularProgressIndicator());
          }
          if (state is WordsLoaded) {
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (state.words.isNotEmpty)
                    ListView.builder(
                      shrinkWrap: true,
                      itemCount: state.words.length,
                      itemBuilder: (context, index) {
                        myList.add(state.words[index]);
                        return _wordCard(state.words[index]);
                      },
                    ),
                  if (state.words.isNotEmpty)
                    Padding(
                      padding: const EdgeInsets.only(bottom: 80),
                      child: Center(
                          child: Text(
                        'S C O R E : $totalScore',
                        style: const TextStyle(fontSize: 16),
                      )),
                    ),
                  if (state.words.isEmpty)
                    const Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: Center(child: Text('USE + TO ADD NEW WORD')),
                    ),
                ],
              ),
            );
          } else {
            return const Text('Someting wrong');
          }
        },
      ),
    );
  }

  Card _wordCard(Word word) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(word.text),
            ),
            const SizedBox(
              width: 10,
            ),
            Container(
                padding: const EdgeInsets.all(15),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.green,
                ),
                child: Text(
                  '${word.score}',
                  style: const TextStyle(color: Colors.white),
                )),
          ],
        ),
      ),
    );
  }
}
