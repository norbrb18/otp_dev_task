import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otp_task/blocs/bloc/words_bloc.dart';
import 'package:otp_task/model/word_model.dart';

class AddWordScreen extends StatefulWidget {
  const AddWordScreen({super.key});

  @override
  State<AddWordScreen> createState() => _AddWordScreenState();
}

class _AddWordScreenState extends State<AddWordScreen> {
  TextEditingController controllerWord = TextEditingController();
  int score = 0;
  List<String> currentlyAdded = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white10,
        title: const Text(
          'A D D   W O R D ',
          style: TextStyle(color: Colors.black),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context, score);
          },
          child: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
      ),
      body: BlocListener<WordsBloc, WordsState>(
        listener: (context, state) {
          if (state is WordsLoaded) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                backgroundColor: Colors.green,
                content: Text('${controllerWord.text} is added'),
              ),
            );
          }
        },
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                TextFormField(
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp("[a-zA-Z]")),
                  ],
                  keyboardType: TextInputType.name,
                  decoration: const InputDecoration(
                      hintText: 'Write a new word', fillColor: Colors.black),
                  controller: controllerWord,
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: () {
                    if (controllerWord.text != '' &&
                        controllerWord.text.length <
                            'pneumonoultramicroscopicsilicovolcanoconiosis'
                                .length &&
                        !currentlyAdded.contains(controllerWord.text)) {
                      var word = Word(
                          text: controllerWord.text,
                          score: controllerWord.text.length);
                      context.read<WordsBloc>().add(AddWords(word: word));
                      score += controllerWord.text.length;
                      currentlyAdded.add(controllerWord.text);
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          backgroundColor: Colors.red,
                          content: Text('Please type a valid word!'),
                        ),
                      );
                    }
                  },
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.black,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 10),
                      textStyle: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold)),
                  child: const Text('A D D'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
