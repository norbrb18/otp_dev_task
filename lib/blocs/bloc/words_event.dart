part of 'words_bloc.dart';

abstract class WordsEvent extends Equatable {
  const WordsEvent();

  @override
  List<Object> get props => [];
}

class LoadWords extends WordsEvent {
  final List<Word> words;

  const LoadWords({this.words = const <Word>[]});
  @override
  List<Object> get props => [words];
}

class AddWords extends WordsEvent {
  final Word word;

  const AddWords({required this.word});
  @override
  List<Object> get props => [word];
}
