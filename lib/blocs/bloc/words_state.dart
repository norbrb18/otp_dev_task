part of 'words_bloc.dart';

abstract class WordsState extends Equatable {
  const WordsState();

  @override
  List<Object> get props => [];
}

class WordsLoading extends WordsState {}

class WordsLoaded extends WordsState {
  final List<Word> words;

  const WordsLoaded({this.words = const <Word>[]});

  @override
  List<Object> get props => [words];
}
