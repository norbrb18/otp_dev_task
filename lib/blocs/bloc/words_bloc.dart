import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otp_task/model/word_model.dart';

part 'words_event.dart';
part 'words_state.dart';

class WordsBloc extends Bloc<WordsEvent, WordsState> {
  WordsBloc() : super(WordsLoading()) {
    on<LoadWords>(_onLoadWords);
    on<AddWords>(_onAddWords);
  }
  void _onLoadWords(LoadWords event, Emitter<WordsState> emit) {
    emit(WordsLoaded(words: event.words));
  }

  void _onAddWords(AddWords event, Emitter<WordsState> emit) {
    final state = this.state;
    if (state is WordsLoaded) {
      emit(WordsLoaded(words: List.from(state.words)..add(event.word)));
    }
  }
}
