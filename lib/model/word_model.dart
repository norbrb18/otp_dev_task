
import 'package:equatable/equatable.dart';

class Word extends Equatable {
  final String text;
  final int score;

  const Word({required this.text, this.score = 0});

  @override
  List<Object?> get props => [
        text,
        score,
      ];
}
